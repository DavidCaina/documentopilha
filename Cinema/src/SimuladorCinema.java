import java.io.FileNotFoundException; 
import java.io.FileReader; 
import java.io.IOException; 

import org.json.simple.JSONObject; 
import org.json.simple.parser.JSONParser; 
import org.json.simple.parser.ParseException;
import org.json.simple.JSONObject;

/*
 *  
 * Programa principal da simulacao
 */
public class SimuladorCinema
{
	
    public static void main(String[] args)
    {
    	JSONObject arq;
    	JSONParser parser = new JSONParser();
    	
    	Long limiteImax1 = null;
    	Long limite3D1 = null; 
    	Long limiteComum1 = null;
    	Long duracao1 = null;
    	double probabilidadedeChegada=0;
    	try{
    		arq = (JSONObject) parser.parse(new FileReader ("arquivo.json"));
    		limiteImax1 =  (Long) arq.get("limiteImax");
    		limite3D1 = (Long) arq.get("limite3D");
    		limiteComum1 = (Long) arq.get("limiteComum");
    		probabilidadedeChegada = (Double) arq.get("probabilidadeDeChegada");
    		duracao1 = (Long) arq.get("duracao");
    	}
    	catch(FileNotFoundException e1){
    		System.out.println("Erro de E/S"); 
    	}
    	catch(IOException e){
    		System.out.println("Erro de E/S"); 
    	}
    	
    	catch(ParseException e){
    		System.out.println("Erro de E/S"); 
    	}
    	Integer limiteImax= new Integer(limiteImax1.intValue());
    	Integer limite3D= new Integer(limite3D1.intValue());
    	Integer limiteComum= new Integer(limiteComum1.intValue());
    	Integer duracao= new Integer(duracao1.intValue());
        Simulacao sim = new Simulacao(true,limiteImax,limite3D,limiteComum,probabilidadedeChegada,duracao);
        sim.simular();
        sim.imprimirResultados();
    }
}
